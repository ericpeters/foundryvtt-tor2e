import Tor2eRollDialogProcessor from "./Tor2eRollDialogProcessor.js";
import {tor2eUtilities} from "../utilities.js";

export class Tor2eRollDialog extends Dialog {

    /** @override
     *  @inheritdoc
     */
    constructor(data, options) {
        super(data, options);
    }

    /** @override
     *  @inheritdoc
     */
    getData(options) {
        return super.getData(options);
    }

    /** @override
     *  @inheritdoc
     */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ["tor2e", "sheet", "dialog"],
            width: 650,
            height: "auto",
            resizable: true

        });
    }

    /** @override
     *  @inheritdoc
     */
    activateListeners(html) {
        super.activateListeners(html);
        html.find(".toggle").click(tor2eUtilities.eventsProcessing.onToggle.bind(this));
    }

    static async create(rollData) {
        let template = `${CONFIG.tor2e.properties.rootpath}/templates/chat/task-check-dialog.hbs`;
        rollData.backgroundImages = CONFIG.tor2e.backgroundImages["dice-roll"];
        let html = await renderTemplate(template, rollData);

        let defaultOptions = Tor2eRollDialog.defaultOptions;
        let options = foundry.utils.mergeObject(defaultOptions, {
            height: rollData.isCharacter ? defaultOptions.height : defaultOptions.height - 125
        });

        return new Promise(resolve => {
            const data = {
                title: game.i18n.format("tor2e.chat.taskCheck.title", {type: rollData.taskType}),
                content: html,
                rollData: rollData,
                buttons: {
                    normal: {
                        label: game.i18n.localize("tor2e.chat.actions.roll"),
                        callback: html => resolve(new Tor2eRollDialogProcessor(html[0].querySelector("form")).process(html))
                    },
                    cancel: {
                        label: game.i18n.localize("tor2e.chat.actions.cancel"),
                        callback: html => resolve({cancelled: true})
                    }
                },
                default: "normal",
                close: () => resolve({cancelled: true})

            }
            new this(data, options).render(true)
        });
    }

}